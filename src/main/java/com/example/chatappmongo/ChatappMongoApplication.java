package com.example.chatappmongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChatappMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChatappMongoApplication.class, args);
    }

}
